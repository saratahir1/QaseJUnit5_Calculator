
## CALCULATOR application.

### In order  to start the appication with GUI:
```
mvn javafx:run
```

### In order to start the application in Terminal:
```
mvn clean compile package
java -jar target/Calculator-1.0-SNAPSHOT.jar
```

### In order to execute the test cases:
```
mvn clean test
```
### In order to execute the test cases and submit to Qase Test runs:
```
mvn clean test  \
    -DQASE_ENABLE=true \
    -DQASE_PROJECT_CODE=CALCULATOR \
    -DQASE_RUN_NAME="Calculator test run" \
    -DQASE_RUN_DESCRIPTION="Run test of calculator on 5 operators and unsupported operator." \
    -DQASE_API_TOKEN=<TOKEN>
```
### Note:
- Please replace `<TOKEN>` with the JUnit5 token generated on Qase App.
