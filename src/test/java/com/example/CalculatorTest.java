package com.example;

import org.junit.jupiter.api.Test;
import io.qase.api.annotation.QaseTitle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.lang.IllegalArgumentException;
import java.lang.ArithmeticException;
import java.lang.Exception;

import com.example.Calculator;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {


    /*
     * Test adding two numbers
     */
    @Test
    @QaseTitle("Test addition of two positive numbers")
    void testAdditionTwoPosNumb()
	{
        double res = 0.0;
        double firstNum = 3.0;
        double secondNum = 2.0;
        char op = '+';
        double expected = 5.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }

    @Test
    @QaseTitle("Test addition of two negative numbers")
    void testAdditionTwoNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = -2.0;
        char op = '+';
        double expected = -5.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    @Test
    @QaseTitle("Test addition of a positive number and a negative number")
    void testAdditionPosAndNegNumber()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = 2.0;
        char op = '+';
        double expected = -1.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    /*
     * Test substracting two numbers
     */
    @Test
    @QaseTitle("Test substraction of two positive numbers")
    void testSubstractionTwoPosNumb()
	{
        double res = 0.0;
        double firstNum = 3.0;
        double secondNum = 2.0;
        char op = '-';
        double expected = 1.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }

    @Test
    @QaseTitle("Test substraction of two negative numbers")
    void testSubstractionTwoNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = -2.0;
        char op = '-';
        double expected = -1.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }

    @Test
    @QaseTitle("Test substraction of a positive number and a negative number")
    void testSubstractionPosNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = 2.0;
        char op = '-';
        double expected = -5.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }

    /*
     * Test multiplying two numbers
     */
    @Test
    @QaseTitle("Test multiplitcation of two positive numbers")
    void testMultiplicationTwoPosNumb()
	{
        double res = 0.0;
        double firstNum = 3.0;
        double secondNum = 2.0;
        char op = '*';
        double expected = 6.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    @Test
    @QaseTitle("Test multiplitcation of two negative numbers")
    void testMultiplicationTwoNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = -2.0;
        char op = '*';
        double expected = 6.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    @Test
    @QaseTitle("Test multiplitcation of a positive number and a negative number")
    void testMultiplicationPosNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = 2.0;
        char op = '*';
        double expected = -6.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    @Test
    @QaseTitle("Test multiplitcation with zero")
    void testMultiplicationWithZero()
	{
        double res = 0.0;
        double firstNum = 100.0;
        double secondNum = 0.0;
        char op = '*';
        double expected = 0.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test multiplitcation zero with zero")
    void testMultiplicationZeroWithZero()
	{
        double res = 0.0;
        double firstNum = 0.0;
        double secondNum = 0.0;
        char op = '*';
        double expected = 0.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }


    /*
     * Test dividing two numbers
     */
    @Test
    @QaseTitle("Test division two positive numbers")
    void testDivisionTwoPosNumb()
	{
        double res = 0.0;
        double firstNum = 3.0;
        double secondNum = 2.0;
        char op = '/';
        double expected = 1.5;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test division of two negative numbers")
    void testDivisionTwoNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = -2.0;
        char op = '/';
        double expected = 1.5;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test division of a positive number and a negative number")
    void testDivisionPosNegNumb()
	{
        double res = 0.0;
        double firstNum = -3.0;
        double secondNum = 2.0;
        char op = '/';
        double expected = -1.5;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test division by zero")
    void testDivisionByZero()
	{
        double firstNum = -3.0;
        double secondNum = 0.0;
        char op = '/';
        Calculator cal = new Calculator();

        assertThrows(ArithmeticException.class, () -> cal.doCalculation(firstNum, secondNum, op));
    }
    @Test
    @QaseTitle("Test division zero by zero")
    void testDivisionZeroByZero()
	{
        double firstNum = 0.0;
        double secondNum = 0.0;
        char op = '/';
        Calculator cal = new Calculator();

        assertThrows(ArithmeticException.class, () -> cal.doCalculation(firstNum, secondNum, op));
    }

    /*
     * Test exponenting two numbers
     */
    @Test
    @QaseTitle("Test exponent a positive number by a positive number")
    void testExponentPosTwoNumb()
	{
        double res = 0.0;
        double firstNum = 3.0;
        double secondNum = 2.0;
        char op = '^';
        double expected = 9;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent a positive number by a negative number")
    void testExponentPosByNegNumb()
	{
        double res = 0.0;
        double firstNum = 4.0;
        double secondNum = -2.0;
        char op = '^';
        double expected = 0.0625;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent a negative number by a positive number")
    void testExponentNegByPosNumb()
	{
        double res = 0.0;
        double firstNum = -4.0;
        double secondNum = 2.0;
        char op = '^';
        double expected = 16.0;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent a negative number by a negative number")
    void testExponentNegByNegNumb()
	{
        double res = 0.0;
        double firstNum = -4.0;
        double secondNum = -3.0;
        char op = '^';
        double expected = -0.015625;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent a decimal number by a decimal number")
    void testExponentDecByDecNumb()
	{
        double res = 0.0;
        double firstNum = 1.2;
        double secondNum = 3.4;
        char op = '^';
        double expected = 1.858729691979481;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent a number by zero")
    void testExponentByZero()
	{
        double res = 0.0;
        double firstNum = 1.2;
        double secondNum = 0;
        char op = '^';
        double expected = 1.0;
        Calculator cal = new Calculator();
        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }
    @Test
    @QaseTitle("Test exponent zero by zero")
    void testExponentZeroByZero()
	{
        double res = 0.0;
        double firstNum = 0.0;
        double secondNum = 0;
        char op = '^';
        double expected = 1.0;
        Calculator cal = new Calculator();

        try
        {
            res = cal.doCalculation(firstNum, secondNum, op);
        }
        catch(Exception e){};

		assertEquals(expected , res);
    }

    /*
     * Testing unsupported operator
     */
    @Test
    @QaseTitle("Test unsupported operator")
    void testExponentUnsupportedOperator()
	{
        double firstNum = 0.0;
        double secondNum = 0;
        char op = '%';
        Calculator cal = new Calculator();

        assertThrows(IllegalArgumentException.class, () -> cal.doCalculation(firstNum, secondNum, op));
    }



    // @TestTemplate
    // public void exampleTest() {
    //     // Perform test actions
    //     int actual = 2 + 2;

    //     // Define test result details
    //     String title = "Example Test";
    //     String description = "Test adding numbers";
    //     ResultStatus status = ResultStatus.PASSED;
    //     LocalDateTime startedAt = LocalDateTime.now().minusMinutes(5);
    //     LocalDateTime finishedAt = LocalDateTime.now();
    //     String[] attachments = {"https://example.com/screenshot.png"};

    //     // Add test result to Qase
    //     TestRunResultAddRequest resultRequest = new TestRunResultAddRequest(
    //             title,
    //             description,
    //             status,
    //             startedAt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
    //             finishedAt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
    //             attachments
    //     );
    //     TestRunResultAddResponse resultResponse = qaseApi.testRunResult().add(PROJECT_CODE, TEST_RUN_CODE, resultRequest);
    //     long resultId = resultResponse.getResult().getId();

    //     // Get test result from Qase
    //     TestRunResultResponse testResult = qaseApi.testRunResult().get(PROJECT_CODE, TEST_RUN_CODE, resultId);

    //     // Verify test result
    //     assertEquals(ResultStatus.PASSED, testResult.getResult().getStatus());
    //     assertEquals(title, testResult.getResult().getTitle());
    // }
}
