package com.example;
//sample com.example on JavaFX GUI
import javafx.application.Application ;
import javafx.scene.Group ;
import javafx.scene.Scene ;
import javafx.stage.Stage ;
import javafx.scene.text.Font ;
import javafx.scene.text.FontPosture ;
import javafx.scene.text.FontWeight ;
import javafx.scene.text.Text ;

import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;

import java.util.*;
import java.lang.IllegalArgumentException;

import com.example.Calculator;
//main class


public class GUI extends Application {
    private String input = "0";
    private double result = 0.0;
    private EventHandler<ActionEvent> quiter = new EventHandler<ActionEvent>() {
        public void handle(ActionEvent e)
        {
            System.exit(0);
        }
    };

    private EventHandler<ActionEvent> doCalculate(Text resultField) {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ev)
            {
                // Scanner scanner = new Scanner(System.in);
                Calculator cal = new Calculator();
                try
                {
                    char op = 'c';
                    double firstNum = 0, secondNum = 0;
                    boolean isFirst = true;

                    for (char ch : input.toCharArray()) {
                        if (!Character.isDigit(ch)) {
                            if (isFirst) {
                                isFirst = false;
                                op = ch;
                            }
                            else {
                                Alert a = new Alert(AlertType.NONE);
                                a.setAlertType(AlertType.CONFIRMATION);
                                a.setContentText("INVALID OPERATOR! Please use one operator with two number only.");
                                a.show();
                                throw new IllegalArgumentException("");
                            }
                        } else {
                            int intVal = ch - '0';
                            if (isFirst) {
                                firstNum = firstNum * 10 + intVal;
                            } else {
                                secondNum = secondNum * 10 + intVal;
                            }
                        }
                    }
                    result = cal.doCalculation(firstNum, secondNum, op);
                }
                catch (ArithmeticException e)
                {
                    System.out.println("Divided by zero operation cannot be possible");
                }
                catch (IllegalArgumentException ill) {
                    System.out.println("Illegal operator, please use +, -, *, / ^:");
                }
                catch(Exception e)
                {
                    System.out.println("Exception occur, the program exit!");
                }
                setTextForTextField(resultField, "Result \t=\t " + Double.toString(result));
            }
        };
    };

    public void println(String text) {
        System.out.println(text);
    }

    public void print(String text) {
        System.out.print(text);
    }

    public void setTextForTextField(Text tf, String text) {
        if (text == "")
            tf.setText("0");
        else
            tf.setText(text);
    }

    public void addInput(String ch, Text tf) {
        if (input == "0")
            input = ch;
        else
            this.input = this.input + ch;
        this.setTextForTextField(tf, "Input \t=\t " + this.input);
    }


    public EventHandler<ActionEvent> eventOnInput(String str, Text inputString) {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
                addInput(str, inputString);
            }
        };
    }


    public EventHandler<ActionEvent> clearLast(Text tf) {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
                if (input.length() >= 1)
                    input = input.substring(0, input.length()-1);
                if (input.length() < 1)
                    input = "0";
                setTextForTextField(tf, "Input \t=\t " + input);
            }
        };

    }
    @Override
    public void start(Stage st) {
        // create a stack pane
        Pane pane = new Pane();

        // create a textfield
        Text inputString = new Text();
        this.setTextForTextField(inputString, "Input \t=\t " + this.input);

        pane.getChildren().add(inputString);
        inputString.relocate(20, 20);

        Text resultString = new Text();
        this.setTextForTextField(resultString, "Result \t=\t " + Double.toString(this.result));
        pane.getChildren().add(resultString);
        resultString.relocate(20, 40);

        for (int i = 1; i <= 10; i++) {
            String str = Integer.toString(i%10);
            Button b = new Button(str);
            b.setOnAction(this.eventOnInput(str, inputString));
            pane.getChildren().add(b);
            b.relocate(100 + ((i-1)%3) * 30, 100 + ((i-1)/3) * 30); // x, y
        }
        String[] operator = {"+", "-", "*", "/", "^"};
        for(int i = 0; i < 5; i++) {
            String op = operator[i];
            Button b = new Button(op);
            b.setOnAction(this.eventOnInput(op, inputString));
            pane.getChildren().add(b);
            b.relocate(190, 100 + i * 30); // x, y
        }

        Button clear = new Button("Clear");
        clear.setOnAction(this.clearLast(inputString));
        pane.getChildren().add(clear);
        clear.relocate(130, 220);


        Button calculate = new Button("Calculate");
        calculate.setOnAction(this.doCalculate(resultString));
        pane.getChildren().add(calculate);
        calculate.relocate(130, 260);

        Button quit = new Button("Quit");
        quit.setOnAction(this.quiter);
        pane.getChildren().add(quit);
        quit.relocate(250, 290);

        // create a scene
        Scene sc = new Scene(pane, 300, 350);

        // set the scene
        st.setScene(sc);
        st.show();
    }
    //main method
    public static void main(String args[])
    {
        System.out.println("STARTING");
        //launch the application
        launch(args);
    }
}