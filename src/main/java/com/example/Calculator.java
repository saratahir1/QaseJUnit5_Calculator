package com.example;

import java.lang.IllegalArgumentException;
import java.util.Scanner;

public class Calculator {

    public Calculator(){};

    public double doCalculation(double firstNum, double secondNum, char op) throws ArithmeticException, IllegalArgumentException { // TODO: test this method
        double result = 0;

        switch (op) {
            case '+':
                result = firstNum + secondNum;
                break;
            case '-':
                result = firstNum - secondNum;
                break;
            case '*':
                result = firstNum * secondNum;
                break;
            case '/':
                    if (secondNum == 0.0) {
                        throw new ArithmeticException("Division by zero is prohibited!");
                    }
                    result = firstNum / secondNum;
                break;
            case '^':
                result = Math.pow(firstNum, secondNum);
                break;
            // case .. further operator here
            default:
                throw new IllegalArgumentException("Illegal operator");
        }

        return result;
    }

    public void println(String text) {
        System.out.println(text);
    }

    public void print(String text) {
        System.out.print(text);
    }

    public double terminalCacluator(Scanner scan)
    {
        print("Input operator +, -, *, /, ^: ");
        String op = scan.nextLine();
        print("Input the first number: ");
        double firstNum = scan.nextDouble();
        print("Input the second number: ");
        double secondNum = scan.nextDouble();
        return doCalculation(firstNum, secondNum, op.charAt(0));
    }
    public static void main(String[] args)
    {
        try
        {
            Scanner scan = new Scanner(System.in);
            Calculator cal = new Calculator();
            double res = cal.terminalCacluator(scan);
            System.out.println("Result: " + res);
        }
        catch (Exception e)
        {
            System.out.println("Error while executing the application: " + e.getMessage());
        }
    }
}
