## Testing strategy (based on equivalence classes partitioning method):

### Addition
    1.  Add two positive number
    2.  Add two negative number
    3.  Add positive and negative

### Substraction
    1.  Substract two positive number
    2.  Substract two negative number
    3.  Substract positive and negative

### Multiplication
    1.  Multiple two positive number
    2.  Multiple two negative number
    3.  Multiple positive and negative
    4.  Multiple with zero
    5.  Multiple zero with zero

### Division
    1.  Divide two positive number
    2.  Divide two negative number
    3.  Divide positive and negative
    4.  Divide with zero
    5.  Divide zero with zero

### Exponent
    1.  Power a positive by positive
    2.  Power negative by positive
    3.  Power positive by negative
    4.  Power negative by negative
    5.  Power a decimal by a decimal
    6.  Power by zero
    7.  Power zero by zero

### Other operator
    1.  Unsupported operator (other than +, -, *, /, ^)

Total: 24 test cases.
